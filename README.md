# Overview of (engineering) research repositories and databases

Currently, the following information is collected:

- Hosting Institution: Who is responsible for the service?
- Name
- Type: Is it a (institutional) repository, a database, etc.?
- Subject Area
- Open Access: Is the data published open access (--> yes) or are datasets published where restrictions apply (--> partially)?
- API: Is there a programmatic interface to access the service? In most cases this is a REST API.
- Publication cost
- Dataset size limit
- URL
- Comment

## Development

Development happens on [GitHub](https://github.com/kit-data-manager/Data-Collections-Explorer). There you will find the most current version, as well as everything necessary for deployment.
